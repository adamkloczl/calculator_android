package example.calculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buBackSpace.text = "<-"
    }

    fun showText(view: View){
        if(isNewOp) panel.text = ""
        isNewOp = false
        val buSelect = view as Button
        var stringDisplayed:String = panel.text.toString()
        when(buSelect.id){
            bu0.id -> stringDisplayed += "0"
            bu1.id -> stringDisplayed += "1"
            bu2.id -> stringDisplayed += "2"
            bu3.id -> stringDisplayed += "3"
            bu4.id -> stringDisplayed += "4"
            bu5.id -> stringDisplayed += "5"
            bu6.id -> stringDisplayed += "6"
            bu7.id -> stringDisplayed += "7"
            bu8.id -> stringDisplayed += "8"
            bu9.id -> stringDisplayed += "9"
            buDot.id -> {
                if(stringDisplayed != ""){
                    if(stringDisplayed.last() != '.'){
                        stringDisplayed += "."
                    }
                }
            }
            buPlusMin.id -> {
                if(stringDisplayed != ""){
                    stringDisplayed = if(stringDisplayed.first() != '-'){
                        "-" + stringDisplayed
                    } else{
                        stringDisplayed.substring(1, stringDisplayed.length)
                    }
                }
            }
            buBackSpace.id -> if(stringDisplayed != ""){
                stringDisplayed = stringDisplayed.substring(0,stringDisplayed.length-1)
            }
            buAC.id -> stringDisplayed = ""
            buPercent.id -> {
                var convertedString:Double = stringDisplayed.toDouble()
                convertedString /= 100
                stringDisplayed = convertedString.toString() + "%"
            }
            buDiv.id ->
                if(stringDisplayed != ""){
                stringDisplayed = if(stringDisplayed.last() != '/' || stringDisplayed.last() != 'x' || stringDisplayed.last() != '.' ){
                    "/"
                } else{
                    stringDisplayed.substring(1, stringDisplayed.length)
                }
            }
            buMult.id -> stringDisplayed += "x"
            buSub.id -> stringDisplayed += "-"
            buSum.id -> stringDisplayed += "+"
        }
        panel.text = stringDisplayed
    }

    var op = ArrayList<String>()
    var previousNumber = ArrayList<String>()
    var isNewOp = true

    fun buOpEvent(view:View){
        if(panel.text == "") return
        val buSelect = view as Button
        when(buSelect.id) {
            buMult.id -> op.add("*")
            buDiv.id -> op.add("/")
            buSub.id -> op.add("-")
            buSum.id -> op.add("+")
        }
        previousNumber.add(panel.text.toString())
        isNewOp = true
    }

    fun buEqualEvent(view:View){
        if(panel.text == "") return
        previousNumber.add(panel.text.toString())
        var finalNumber:Double = previousNumber[0].toDouble()
        previousNumber.removeAt(0)

        for(x in 0 until previousNumber.count()){
            when(op[x]){
                "*" -> finalNumber *= previousNumber[x].toDouble()
                "/" -> finalNumber /= previousNumber[x].toDouble()
                "+" -> finalNumber += previousNumber[x].toDouble()
                "-" -> finalNumber -= previousNumber[x].toDouble()
            }
        }

        panel.text = finalNumber.toString()
        previousNumber.clear()
        op.clear()
        isNewOp = true
    }

}